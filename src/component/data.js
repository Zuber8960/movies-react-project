import movies from "../assets/movies.json";

const newMoviesData = movies.map(({ ...movie }) => {
  movie.currentStatus = "watched";
  return movie;
});

export default newMoviesData;