import React, { useState } from "react";
// import data from "../assets/movies.json";
import data from "./data";
import EachMovie from "./EachMovie";

const Movies = () => {
  const [movies, setMovies] = useState(data);

  return (
    <>
      <EachMovie movieStatus="watched" setMovies={setMovies} movies={movies}/>
      <EachMovie movieStatus="not-watched" setMovies={setMovies} movies={movies}/>
      <EachMovie movieStatus="will-watched" setMovies={setMovies} movies={movies}/>
    </>
  );
};

export default Movies;
