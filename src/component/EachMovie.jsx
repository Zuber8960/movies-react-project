import React from "react";

const EachMovie = ({ movieStatus, setMovies, movies }) => {

  function toggleStatus(event) {
    console.log(event.target.id, event.target.value);

    const result = movies.reduce((arr, { ...movie }) => {
      if (movie.Rank === +event.target.id) {
        movie.currentStatus = event.target.value;
      }
      arr.push(movie);
      return arr;
    }, []);

    setMovies(result);
  }


  return (
    <>
      <h1>{movieStatus}</h1>
      {movies.length &&
        movies
          .filter((movie) => movie.currentStatus === movieStatus)
          .map((movie) => {
            return (
              <div key={movie.Rank}>
                <h4>{movie.Title}</h4>

                <select id={movie.Rank} onChange={toggleStatus}>
                  <option value="watched">watched</option>
                  <option value="not-watched">not-watched</option>
                  <option value="will-watched">will-watched</option>
                </select>
              </div>
            );
          })}
    </>
  );
};

export default EachMovie;
